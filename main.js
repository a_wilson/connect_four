var player1Name = window.prompt("Player 1, please enter your name: ", "Billy Bragg");
var player2Name = window.prompt("Player 2, please enter your name: ", "Ronald Weasley");

var player1 = {
    name: player1Name,
    color: 'rgb(86, 151, 255)' // blue
};

var player2 = {
    name: player2Name,
    color: 'rgb(237, 45, 73' // red
};

var table = $('table tr'); // jquery selector

function  winStatus(rowNum, colNum) {
    console.log("You won, starting at this row,col");
    console.log(rowNum);
    console.log(colNum);
    alert("You won, starting at (row,col): " + rowNum + "," + colNum + ".");
}

function debug(rowIndex,colIndex) {
    table.eq(rowIndex).find('td').eq(colIndex).find('button').hide()
    table.eq(rowIndex).find('td').eq(colIndex).find('button').show()
}

function changeColor(rowIndex,colIndex, color){
    return table.eq(rowIndex).find('td').eq(colIndex).find('button').css('background-color',color)
}

function whatColor(rowIndex,colIndex){
    // console.log("Reporting on " + rowIndex + "," + colIndex)
    return table.eq(rowIndex).find('td').eq(colIndex).find('button').css('background-color')
}

function checkBottom(colIndex) {
    var colorReport = whatColor(5,colIndex);
    for (var i = 5; i > -1; i--) {
        color = whatColor(i,colIndex);
        if (color === 'rgb(128, 128, 128)') {
            return i
        }
    }
}

// Check to see if 4 inputs are the same color
function colorMatchCheck(one,two,three,four){
    return (one===two && one===three && one===four && one !== 'rgb(128, 128, 128)' && one !== undefined);
  }

function horizontalWinCheck() { // Checking to see if there's a color match along the rows
    console.log("Checking horizontal")
    for (var row = 0; row < 6; row++) { 
        for (var col = 0; col < 4; col++) { // Starting at each coloumn in turn, moving horizontally.
            console.log(col + " " + row)
            console.log(colorMatchCheck(whatColor(row,col), whatColor(row,col+1), whatColor(row,col+2), whatColor(row,col+3)))
            if (colorMatchCheck(whatColor(row,col), whatColor(row,col+1), whatColor(row,col+2), whatColor(row,col+3) == true)) {
                console.log('Horizontal');
                winStatus(row,col);
                return true;
            } else {
                continue;
            }

        }
    }
}

function verticalWinCheck() {
    console.log("Checking vertical")
    for (var col = 0; col < 7; col++) {
        for (var row = 0; row < 3; row++) {
            console.log(col + " " + row)
            console.log((colorMatchCheck(whatColor(row,col), whatColor(row+1,col), whatColor(row+2,col), whatColor(row+3,col))))
            if (colorMatchCheck(whatColor(row,col), whatColor(row+1,col), whatColor(row+2,col), whatColor(row+3,col) == true)) {
                console.log('Vertical');
                winStatus(row,col);
                return true;
            } else {
                continue;
            }
        }
    }
}

// TODO: Add diagonal win checks


// Always start with player 1
var currentTurn = player1;

function setText(player) {
    $("#player").text(player);
}

setText(currentTurn.name);

$('.board button').on('click', function(){
    console.log("Click.")

    var col = $(this).closest('td').index();

    var bottomAvail = checkBottom(col);

    changeColor(bottomAvail,col,currentTurn.color);
    if (horizontalWinCheck() || verticalWinCheck()) {
        alert(currentTurn.name + " You have won!")
    }

    window.currentTurn = (currentTurn == player1) ? player2:player1

    setText(currentTurn.name)

})
